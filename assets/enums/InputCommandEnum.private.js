"use strict";

const InputCommandEnum = {
    NONE: 0,
    BROADCAST: 1,
    HELP: 2,
    SUBSCRIBE: 3
};

Object.freeze(InputCommandEnum);
module.exports = InputCommandEnum;
